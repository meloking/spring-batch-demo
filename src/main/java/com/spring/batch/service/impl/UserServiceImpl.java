package com.spring.batch.service.impl;

import com.spring.batch.dao.cluster.CityDao;
import com.spring.batch.dao.master.UserDao;
import com.spring.batch.entity.City;
import com.spring.batch.entity.User;
import com.spring.batch.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author linjianhao
 * @description
 * @className UserServiceImpl
 * @date 2019-05-18
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * 主数据源
     */
    @Autowired
    private UserDao userDao;
    /**
     * 从数据源
     */
    @Autowired
    private CityDao cityDao;

    @Override
    public User findByName(String userName) {
        User user = userDao.findByName(userName);
        City city = cityDao.findByName("温岭市");
        user.setCity(city);
        return user;
    }
}
