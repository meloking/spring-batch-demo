package com.spring.batch.service;

import com.spring.batch.entity.User;

/**
 * @author linjianhao
 * @description
 * @className UserService
 * @date 2019-05-18
 */
public interface UserService {

    User findByName(String userName);

}
