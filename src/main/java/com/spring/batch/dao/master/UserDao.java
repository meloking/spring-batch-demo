package com.spring.batch.dao.master;

import com.spring.batch.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author linjianhao
 * @description
 * @className UserDao
 * @date 2019-05-18
 */
@Mapper
public interface UserDao {

    /**
     * 根据用户名获取用户信息
     *
     * @param userName
     * @return
     */
    User findByName(@Param("userName") String userName);

}
