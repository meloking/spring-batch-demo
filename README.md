#### 一、SpringBoot之整合Mybatis实现Druid多数据源详解
   **1、引入相关jar包：**
   ```
   <!--Spring Boot Mybatis 依赖 -->
           <dependency>
               <groupId>org.mybatis.spring.boot</groupId>
               <artifactId>mybatis-spring-boot-starter</artifactId>
               <version>1.3.1</version>
           </dependency>
           <!-- MySQL 连接驱动依赖 -->
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
               <version>${mysql.version}</version>
           </dependency>
           <!--数据库连接池-->
           <dependency>
               <groupId>com.alibaba</groupId>
               <artifactId>druid</artifactId>
               <version>1.0.28</version>
           </dependency>
   ```
   详细jar可参考pom文件
   **2、数据库准备：**
   ```sql
   ##创建主库
    CREATE DATABASE db_master;
    
    DROP TABLE IF EXISTS `user`;
    CREATE TABLE user
    (
    id INT(10) unsigned PRIMARY KEY NOT NULL COMMENT '用户编号' AUTO_INCREMENT,
    user_name VARCHAR(25) COMMENT '用户名称',
    description VARCHAR(25) COMMENT '描述'
    )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
    
    INSERT user VALUES (1 ,'泥瓦匠','他有一个小网站 bysocket.com');
    
    
    ##创建从库
    
    CREATE DATABASE db_cluster;
    
    DROP TABLE IF EXISTS `city`;
    CREATE TABLE `city` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '城市编号',
    `province_id` int(10) unsigned NOT NULL COMMENT '省份编号',
    `city_name` varchar(25) DEFAULT NULL COMMENT '城市名称',
    `description` varchar(25) DEFAULT NULL COMMENT '描述',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
    
    INSERT city VALUES (1 ,1,'温岭市','BYSocket 的家在温岭。');
    
 ```
   **3、创建主库、从库配置文件：**
   - MasterDataSourceConfig
   ```java
package com.spring.batch.config;
   
   import com.alibaba.druid.pool.DruidDataSource;
   import org.apache.ibatis.session.SqlSessionFactory;
   import org.mybatis.spring.SqlSessionFactoryBean;
   import org.mybatis.spring.annotation.MapperScan;
   import org.springframework.beans.factory.annotation.Qualifier;
   import org.springframework.beans.factory.annotation.Value;
   import org.springframework.context.annotation.Bean;
   import org.springframework.context.annotation.Configuration;
   import org.springframework.context.annotation.Primary;
   import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
   import org.springframework.jdbc.datasource.DataSourceTransactionManager;
   
   import javax.sql.DataSource;
   
   /**
    * @author linjianhao
    * @description
    * @className MasterDataSourceConfig
    * @date 2019-05-18
    */
   @Configuration
   // 扫描 Mapper 接口并容器管理
   @MapperScan(basePackages = MasterDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "masterSqlSessionFactory")
   public class MasterDataSourceConfig {
   
       // 精确到 master 目录，以便跟其他数据源隔离
       static final String PACKAGE = "com.spring.batch.dao.master";
       static final String MAPPER_LOCATION = "classpath:mapper/master/*.xml";
   
       @Value("${spring.datasource.druid.master-data-source.url}")
       private String url;
   
       @Value("${spring.datasource.druid.master-data-source.username}")
       private String user;
   
       @Value("${spring.datasource.druid.master-data-source.password}")
       private String password;
   
       @Value("${spring.datasource.driverClassName}")
       private String driverClass;
   
       @Bean(name = "masterDataSource")
       @Primary
       public DataSource masterDataSource() {
           DruidDataSource dataSource = new DruidDataSource();
           dataSource.setDriverClassName(driverClass);
           dataSource.setUrl(url);
           dataSource.setUsername(user);
           dataSource.setPassword(password);
           return dataSource;
       }
   
       /**
        * 一库事务管理器
        * @return
        */
       @Bean(name = "masterTransactionManager")
       @Primary
       public DataSourceTransactionManager masterTransactionManager() {
           return new DataSourceTransactionManager(masterDataSource());
       }
   
       @Bean(name = "masterSqlSessionFactory")
       @Primary
       public SqlSessionFactory masterSqlSessionFactory(@Qualifier("masterDataSource") DataSource masterDataSource)
               throws Exception {
           final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
           sessionFactory.setDataSource(masterDataSource);
           sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                   .getResources(MasterDataSourceConfig.MAPPER_LOCATION));
           return sessionFactory.getObject();
       }
   }
```
   - ClusterDataSourceConfig
   ```java
package com.spring.batch.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author linjianhao
 * @description
 * @className ClusterDataSourceConfig
 * @date 2019-05-18
 */
@Configuration
// 扫描 Mapper 接口并容器管理
@MapperScan(basePackages = ClusterDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "clusterSqlSessionFactory")
public class ClusterDataSourceConfig {

    // 精确到 cluster 目录，以便跟其他数据源隔离
    static final String PACKAGE = "com.spring.batch.dao.cluster";
    static final String MAPPER_LOCATION = "classpath:mapper/cluster/*.xml";

    @Value("${spring.datasource.druid.cluster-data-source.url}")
    private String url;

    @Value("${spring.datasource.druid.cluster-data-source.username}")
    private String user;

    @Value("${spring.datasource.druid.cluster-data-source.password}")
    private String password;

    @Value("${spring.datasource.driverClassName}")
    private String driverClass;

    @Bean(name = "clusterDataSource")
    public DataSource clusterDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }
    /**
     * 二库事务管理器
     * @return
     */
    @Bean(name = "clusterTransactionManager")
    public DataSourceTransactionManager clusterTransactionManager() {
        return new DataSourceTransactionManager(clusterDataSource());
    }

    @Bean(name = "clusterSqlSessionFactory")
    public SqlSessionFactory clusterSqlSessionFactory(@Qualifier("clusterDataSource") DataSource clusterDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(clusterDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(ClusterDataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }
}
```
  **4、创建对应数据库中的entity、Dao：**
  - User
  - City
  - UserDao（在com.spring.batch.dao.cluster包下）
  - CityDao（在com.spring.batch.dao.master包下）
  
  **5、创建示例接口：**
  接口为：com.spring.batch.controller.UserController.findByName
  
  到此SpringBoot集成多数据源完毕。可访问"http://localhost:8080/api/user?userName=泥瓦匠" 进行验证。
  
  
   
 